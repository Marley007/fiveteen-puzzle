package patnactka;

import java.util.Observable;

public class zasobnik extends Observable {

    private String[][] herniPole;
    private int pocetTahu;
    private boolean konec;

    public zasobnik() {
        this.pocetTahu = 0;
    }

    public String[][] getHerniPole() {
        return herniPole;
    }

    public void setHerniPole(String[][] herniPole) {
        this.herniPole = herniPole;
        this.pocetTahu++;
        setChanged();
        notifyObservers();
    }

    public int getPocetTahu() {
        return pocetTahu;
    }

    public void setPocetTahu(int pocetTahu) {
        this.pocetTahu = pocetTahu;
        setChanged();
        notifyObservers();
    }

    public boolean isKonec() {
        return konec;
    }

    void setKonec(boolean konec) {
        this.konec = konec;
    }

}
