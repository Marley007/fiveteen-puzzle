package patnactka;

import java.util.ArrayList;
import java.util.Random;

public final class jadro {

    private final zasobnik zas;
    private final ArrayList<String> cisla;
    private final String[][] spravneReseni;

    public jadro(zasobnik zas) {
        this.zas = zas;
        this.cisla = new ArrayList<>();
        this.spravneReseni = new String[4][4];

    }

    public void pohyb(int cislo) {
        int[] volnePole = this.getIndexOfFreePos();
        int[] kliknutePole = this.getIndexOfClickedPos(cislo);
        if (this.overMoznostPohybu(volnePole, kliknutePole)) {
            this.prehodPoziceTlacitek(volnePole, kliknutePole, cislo);
            if (this.zjistiJestliNeniKonecHry()) {

                this.zas.setKonec(true);
            }
        }

    }

    public void promichat() {
        System.out.println("micham");
        naplnArrayCisly();
        this.vyplnSpravneReseni();
        this.zas.setHerniPole(this.zamichejPole());
        this.zas.setPocetTahu(0);

    }

    private void naplnArrayCisly() {
        for (int i = 1; i <= 15; i++) {
            this.cisla.add(String.valueOf(i));
        }
        this.cisla.add(" ");
    }

    private String[][] zamichejPole() {
        String pole[][] = new String[4][4];
        Random rnd = new Random();
        for (String[] pole1 : pole) {
            for (int j = 0; j < pole1.length; j++) {
                int index = rnd.nextInt(this.cisla.size());
                pole1[j] = cisla.get(index);
                cisla.remove(index);
            }
        }
        return pole;
    }

    private int[] getIndexOfFreePos() {
        int[] index = new int[2];
        String pole[][] = this.zas.getHerniPole();
        for (int i = 0; i < pole.length; i++) {
            for (int j = 0; j < pole[i].length; j++) {
                if (" ".equals(pole[i][j])) {
                    index[0] = i;
                    index[1] = j;
                }
            }
        }
        return index;
    }

    private int[] getIndexOfClickedPos(int cislo) {
        int[] index = new int[2];
        String pole[][] = this.zas.getHerniPole();
        for (int i = 0; i < pole.length; i++) {
            for (int j = 0; j < pole[i].length; j++) {
                if (String.valueOf(cislo).equals(pole[i][j])) {
                    index[0] = i;
                    index[1] = j;
                }
            }
        }
        return index;
    }

    private boolean overMoznostPohybu(int[] volnaPozice, int[] kliknutaPozice) {

        if ((kliknutaPozice[0] + 1 == volnaPozice[0] || kliknutaPozice[0] - 1 == volnaPozice[0]) && kliknutaPozice[1] == volnaPozice[1]) {
            return true;
        } else {
            return (kliknutaPozice[1] + 1 == volnaPozice[1] || kliknutaPozice[1] - 1 == volnaPozice[1]) && kliknutaPozice[0] == volnaPozice[0];
        }
    }

    private void prehodPoziceTlacitek(int[] volnaPozice, int[] kliknutaPozice, int cislo) {
        String pole[][] = this.zas.getHerniPole();
        pole[volnaPozice[0]][volnaPozice[1]] = String.valueOf(cislo);
        pole[kliknutaPozice[0]][kliknutaPozice[1]] = " ";
        this.zas.setHerniPole(pole);
    }

    private void vyplnSpravneReseni() {
        int cislo = 1;
        for (String[] spravneReseni1 : this.spravneReseni) {
            for (int j = 0; j < spravneReseni1.length; j++) {
                spravneReseni1[j] = String.valueOf(cislo);
                cislo++;
            }
        }
        this.spravneReseni[3][3] = " ";
    }

    private boolean zjistiJestliNeniKonecHry() {
        Boolean jeKonec = true;
        String[][] herniPole = this.zas.getHerniPole();
        for (int i = 0; i < herniPole.length; i++) {
            for (int j = 0; j < herniPole[i].length; j++) {
                if (herniPole[i][j] == null ? this.spravneReseni[i][j] != null : !herniPole[i][j].equals(this.spravneReseni[i][j])) {
                    jeKonec = false;
                    break;
                }
            }
        }
        return jeKonec;
    }

}
